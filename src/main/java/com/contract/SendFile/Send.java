package com.contract.SendFile;

import java.io.File;
import java.io.IOException;

import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class Send {


    public static void sendFile(File fileName, String accountid) throws IOException {
    	Object account = accountid;
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("file", getUserFileResource(fileName));
        bodyMap.add("accountid", account);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);

        RestTemplate restTemplate = new RestTemplate();
        //ResponseEntity<String> response = restTemplate.exchange("http://localhost:8088/v1/api/upload/",
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:8080/api/v1/photoupload",
                HttpMethod.POST, requestEntity, String.class);
        System.out.println("response status: " + response.getStatusCode());
        System.out.println("response body: " + response.getBody());
    }
    public static Resource getUserFileResource(File fileName) throws IOException {
    	
        return new FileSystemResource(fileName);
    }

}
