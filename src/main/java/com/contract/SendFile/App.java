package com.contract.SendFile;

import java.io.File;
import java.io.IOException;

/**
 * Send File to Identification Services
 *
 */
public class App {
	public static void main(String[] args) throws IOException {
		if (args.length == 0) {
			System.out.println("Please input filename");
			return;
		}
		File file = new File(args[0]);
		String accountid = args[1];
		if (file.exists()) {
			String fileName = file.getName();

			System.out.println("Sending File " + fileName);
			Send.sendFile(file, accountid);

		} else {
			System.out.println("File does not exists!");
		}
	}
}
